# CI Stack

Quickly start set of CI tools including:

- Phabricator and Phabricator MySQL

- Jenkins

- Nexus Repository Manager

## System Requirements

- Supported operating systems: Ubuntu 16

- Recommended hardware configuration:
    + RAM 4GB  (or more)
    + CPU Core i5 (or higher)
    + HDD 20GB (or more)

## Installation

- Before installing, you must configure your DNS for your machine to resolve tma domain and ensure internet connectivity during installation time.

- Change value of variables in setup.sh file: HOST_DIR and HOST_IP to appropriate values

- Transfer files: setup.sh, docker-compose.yml to your machine then run below commands:

```
sudo chmod 755 ./setup.sh
sudo ./setup.sh
```

- After a few minutes, the installation process completes successfully, you can access:

```
Phabricator: http://YOUR_HOST_IP_ADDRESS
Jenkins: http://YOUR_HOST_IP_ADDRESS:8080
Artifactory: http://YOUR_HOST_IP_ADDRESS:8081
Sonarqube: http://YOUR_HOST_IP_ADDRESS:9000
```
