#!/bin/sh

export http_proxy=http://192.168.88.57:8080
export https_proxy=http://192.168.88.57:8080
export HOST_DIR=/home/tma/ci-stack
export HOST_IP=172.26.162.52

sed -e 's,HOST_DIR,'$HOST_DIR',g' -i ./docker-compose.yml
sed -e 's,PHABRICATOR_HOST=HOST_IP,PHABRICATOR_HOST='$HOST_IP',g' -i ./docker-compose.yml

sudo mkdir -p $HOST_DIR/phab/repos
sudo mkdir -p $HOST_DIR/phab/extensions
sudo mkdir -p $HOST_DIR/phab/mysql
sudo mkdir -p $HOST_DIR/phab/mycnf
cp mysqld.cnf $HOST_DIR/phab/mycnf/mysqld.cnf
sudo mkdir -p $HOST_DIR/phab/sshkeys
sudo mkdir -p $HOST_DIR/phab/filestore
sudo mkdir -p $HOST_DIR/jenkins
sudo mkdir -p $HOST_DIR/artifactory
sudo mkdir -p $HOST_DIR/sonarqube/conf
sudo mkdir -p $HOST_DIR/sonarqube/data
sudo mkdir -p $HOST_DIR/sonarqube/extensions
sudo mkdir -p $HOST_DIR/sonarqube/bundled-plugins
sudo mkdir -p $HOST_DIR/sonarqube/postgresql
sudo mkdir -p $HOST_DIR/sonarqube/postgresql/data

sudo chmod -R 777 $HOST_DIR/phab/filestore
sudo chmod -R 777 $HOST_DIR/phab/repos

docker stack deploy --with-registry-auth --compose-file docker-compose.yml ci-stack
